# HTML5 Game Engine Comparison

This repo contains 4 projects made in Construct3, Defold, Godot and Unity. They are exported for HTML5 into a 854x480 
viewport with a 256x128px label in the center saying "Hello World" (black 24pt Arial).

The aim is to analyze how easy it is to work with them for a simple Hello World demo, as well as check the initial 
download size with not much content.

All tests have been done on Fedora 35.

## Construct3 | *r285.3 stable*
Used free trial from their [Web Editor](https://editor.construct.net/)

## Defold | *1.3.0*
Installed via [Steam](https://store.steampowered.com/app/1365760/Defold/)

### Issues
* Impossible to specify a system font like Arial, used default font
* Impossible to specify a font size without a font, used default size

## Godot | *3.3.4*
Installed via [Steam](https://store.steampowered.com/app/404790/Godot_Engine/)

### Issues
* Impossible to specify a system font like Arial, used default font
* Impossible to specify a font size without a font, used default size

## Unity | *2020.3.32f1*
Installed via [Unity Hub](https://docs.unity3d.com/hub/manual/InstallHub.html#install-hub-linux)

### Issues
* WebGL build failed because of missing dependencies, solved by running `sudo dnf install ncurses-compat-libs`
* Set compression to gzip because of a build error with brotli

## Firefox Network Analysis
### No Optimized
Captured from builds with default options
#### Construct3
![Construct3](construct-hello-world-not-optimized.png)
#### Defold
![Defold](defold-hello-world-not-optimized.png)
#### Godot
![Godot](godot-hello-world-not-optimized.png)
#### Unity
![Unity](unity-hello-world-not-optimized.png)